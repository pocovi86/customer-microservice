package ar.com.mocriservice.customer.repository;

import ar.com.mocriservice.customer.entity.Customer;
import ar.com.mocriservice.customer.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRespository extends JpaRepository<Customer, Long> {

    Customer findByNumberID(String numberID);
    List<Customer> findByLastName(String lastName);
    List<Customer> findByRegion(Region region);
}
