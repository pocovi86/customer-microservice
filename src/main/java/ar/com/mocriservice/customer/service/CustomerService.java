package ar.com.mocriservice.customer.service;

import ar.com.mocriservice.customer.entity.Customer;
import ar.com.mocriservice.customer.entity.Region;

import java.util.List;

public interface CustomerService {

    /**
     * Devuelve una lista de Customer
     * @return {@link List} of {@link Customer}
     */
    List<Customer> findCustomerAll();

    /**
     * Obtiene una lista de Customer por medio de una Region
     * @param region
     * @return {@link List} of {@link Customer}
     */
    List<Customer> findCustomersByRegion(Region region);

    /**
     * Crea un Customer
     * @param customer
     * @return {@link Customer}
     */
    Customer createCustomer(Customer customer);

    /**
     * Actualiza un Customer
     * @param customer
     * @return {@link Customer}
     */
    Customer updateCustomer(Customer customer);

    /**
     * Elimina un Customer
     * @param customer
     * @return
     */
    Customer deleteCustomer(Customer customer);

    /**
     * Obtiene un Customer
     * @param id
     * @return {@link Customer}
     */
    Customer getCustomer(Long id);
}
