package ar.com.mocriservice.customer.service.impl;

import ar.com.mocriservice.customer.entity.Customer;
import ar.com.mocriservice.customer.entity.Region;
import ar.com.mocriservice.customer.repository.CustomerRespository;
import ar.com.mocriservice.customer.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRespository customerRespository;


    @Override
    public List<Customer> findCustomerAll() {
        return customerRespository.findAll();
    }

    @Override
    public List<Customer> findCustomersByRegion(Region region) {
        return customerRespository.findByRegion(region);
    }

    @Override
    public Customer createCustomer(Customer customer) {
        Customer customerDB = customerRespository.findByNumberID( customer.getNumberID() );
        if (customerDB != null){
            return customerDB;
        }

        customer.setState("CREATED");
        customerDB = customerRespository.save( customer );
        return customerDB;
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        Customer customerDB = customerRespository.findByNumberID( customer.getNumberID() );
        if (customerDB != null){
            return customerDB;
        }

        customerDB.setFirstName( customer.getFirstName() );
        customerDB.setLastName( customer.getLastName() );
        customerDB.setEmail( customer.getEmail() );
        customerDB.setPhotoUrl( customer.getPhotoUrl() );

        return customerRespository.save(customerDB);
    }

    @Override
    public Customer deleteCustomer(Customer customer) {
        Customer customerDB = getCustomer( customer.getId() );
        if (customerDB != null){
            return customerDB;
        }

        customerDB.setState("DELETE");
        return customerRespository.save(customerDB);
    }

    @Override
    public Customer getCustomer(Long id) {
        return customerRespository.findById(id).orElse(null);
    }
}
